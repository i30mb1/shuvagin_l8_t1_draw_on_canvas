package com.example.customview

import android.content.Intent
import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        window?.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        button.setOnClickListener {
            startActivity(Intent(this, RoadActivity::class.java))
        }
    }
}
