package com.example.customview

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.*
import android.os.Handler
import android.util.AttributeSet
import android.util.TypedValue
import android.view.MotionEvent
import android.view.View
import androidx.core.graphics.withRotation
import androidx.core.graphics.withScale
import kotlin.random.Random

class RoadWatcher @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {


    fun Float.dpToPx(): Float {
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            this,
            resources.displayMetrics
        )
    }

    var degree = 0f
    var rainDropList: List<RainDrop> = listOf()
    private val paintDropList = Paint()
        .apply { color = Color.BLACK }
        .apply { style = Paint.Style.STROKE }
        .apply { isAntiAlias = true }
        .apply { strokeWidth = 2f }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        rainDropList.forEach { it.draw(canvas, paintDropList) }
        rainDropList = rainDropList.filter { it.isValid() }

        drawRoad(canvas)
        canvas.withRotation(degree, canvasHalfWidth, canvasHalfHeight) {
            drawCarOnSecondLine(canvas)
        }
        drawFace(canvas)

        postInvalidateOnAnimation()
    }

    var cars = arrayListOf(
        BitmapFactory.decodeResource(resources, R.drawable.car_red),
        BitmapFactory.decodeResource(resources, R.drawable.car_gray),
        BitmapFactory.decodeResource(resources, R.drawable.car_yellow)
    )
    var currentCar = cars.random()
    var movementStartToEnd = true
    var movementOnFirstRow = true
    var canvasHeight = 0f
    var canvasWidth = 0f
    var canvasHalfHeight = 0f
    var canvasHalfWidth = 0f
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        canvasHeight = height.toFloat()
        canvasWidth = width.toFloat()
        canvasHalfHeight = canvasHeight / 2f
        canvasHalfWidth = canvasWidth / 2f


    }

    var roadStrokeWidth = 4f.dpToPx()
    var roadWidth = 100f.dpToPx()
    val paintBlack = Paint().apply {
        color = (Color.BLACK)
        style = Paint.Style.FILL_AND_STROKE
        isAntiAlias = true
    }
    val paintWhite = Paint().apply {
        color = (Color.WHITE)
        style = Paint.Style.FILL_AND_STROKE
        isAntiAlias = true
    }

    private fun drawRoad(canvas: Canvas?) {
        val whiteLine = Path()
        whiteLine.moveTo(0f, canvasHalfHeight)
        whiteLine.lineTo(canvasWidth, canvasHalfHeight)
        //draw road
        canvas?.drawRect(
            0f,
            canvasHalfHeight - roadWidth / 2,
            canvasWidth,
            canvasHalfHeight + roadWidth / 2,
            paintBlack
        )
        //draw white line
        canvas?.drawPath(
            whiteLine,
            Paint().apply {
                style = Paint.Style.STROKE
                isAntiAlias = true
                color = (Color.WHITE)
                strokeWidth = roadStrokeWidth
                pathEffect = DashPathEffect(floatArrayOf(150f, 60f), 0f)
            }
        )

    }

    var drawPlease: (myCanvas: Canvas) -> Unit = {}
    var touchPath = Path()
    var stopTheWorld = false
    private val maxRadius by lazy {
        96f.dpToPx()
    }
    var startDrawRainDrop = true
    override fun onTouchEvent(event: MotionEvent): Boolean {
        val pointerIndex = event.actionIndex
        when (event.actionMasked) {
            MotionEvent.ACTION_DOWN,
            MotionEvent.ACTION_POINTER_DOWN -> {
                startDrawRainDrop = true
                stopTheWorld = true
                repeat(100) {
                    Handler().postDelayed(Runnable {
                        if (startDrawRainDrop)
                            rainDropList += RainDrop(
                                event.getX(pointerIndex),
                                event.getY(pointerIndex),
                                maxRadius
                            )
                    }, it * 100L)
                }

                ValueAnimator.ofFloat(faceSize, 450f.dpToPx()).apply {
                    duration = 300L
                    addUpdateListener {
                        faceSize = it.animatedValue as Float
                    }
                    start()
                }

                ValueAnimator.ofFloat(faceEyeRadius,60f.dpToPx()).apply {
                    duration = 300L
                    addUpdateListener {
                        faceEyeRadius = it.animatedValue as Float
                    }
                    start()
                }
            }
            MotionEvent.ACTION_UP,
            MotionEvent.ACTION_POINTER_UP -> {
                startDrawRainDrop = false
                stopTheWorld = false; invalidate()
                ValueAnimator.ofFloat(faceSize, 150f.dpToPx()).apply {
                    duration = 300L
                    addUpdateListener {
                        faceSize = it.animatedValue as Float
                    }
                    start()
                }
                ValueAnimator.ofFloat(faceEyeRadius,20f.dpToPx()).apply {
                    duration = 300L
                    addUpdateListener {
                        faceEyeRadius = it.animatedValue as Float
                    }
                    start()
                }
            }
            MotionEvent.ACTION_MOVE -> {
                stopTheWorld = true

                drawPlease = {
                    //draw left eye
                    val eye1X = faceXCenter - faceSize / 5 + 5f.dpToPx()
                    val eye1Y = faceYCenter + 50f.dpToPx()
                    it.drawCircle(eye1X, eye1Y, faceEyeRadius, paintWhite)
                    //draw left pupil
                    var angle = getAngle(event.rawX, event.rawY, eye1X, eye1Y)
                    var pinX = eye1X + faceEyeRadius / 2 * Math.sin(Math.toRadians(angle)).toFloat()
                    var pinY = eye1Y + faceEyeRadius / 2 * Math.cos(Math.toRadians(angle)).toFloat()
                    it.drawCircle(pinX, pinY, faceEyeRadius / 2, paintBlack)
                    //draw right eye
                    val eye2X = faceXCenter + faceSize / 5 - 5f.dpToPx()
                    val eye2Y = faceYCenter + 50f.dpToPx()
                    it.drawCircle(eye2X, eye2Y, faceEyeRadius, paintWhite)
                    //draw right pupil
                    angle = getAngle(event.rawX, event.rawY, eye2X, eye2Y)
                    pinX = eye2X + faceEyeRadius / 2 * Math.sin(Math.toRadians(angle)).toFloat()
                    pinY = eye2Y + faceEyeRadius / 2 * Math.cos(Math.toRadians(angle)).toFloat()
                    it.drawCircle(pinX, pinY, faceEyeRadius / 2, paintBlack)
                }
            }
        }
        return true
    }

    fun initialAll() {
        currentCar = cars.random()
        movementStartToEnd = Random.nextBoolean()
        movementOnFirstRow = Random.nextBoolean()
        carSpeed = (10..20 step 5).shuffled().first()
        initialTranslate = carWidth

        when (movementStartToEnd) {
            true -> degree = 180f
            false -> degree = 0f
        }

        invalidate()
    }

    var carSpeed = 10
    var carWidth = -(roadWidth / 2)
    var carXCenter = 0f
    var carYCenter = 0f
    var initialTranslate = carWidth
    private fun drawCarOnSecondLine(canvas: Canvas?) {
        val rectCar = RectF(
            0f + initialTranslate,
            canvasHalfHeight + (roadWidth / 8),
            (roadWidth / 2) + initialTranslate,
            canvasHalfHeight + (roadWidth / 8 * 3)
        )
        carXCenter = rectCar.centerX()
        carYCenter = rectCar.centerY()

        fun moveCarFromBeginingToEnd() {
            initialTranslate += carSpeed
            if (initialTranslate >= (canvasWidth - carWidth) - 100) {
//                initialPoint
                initialAll()
            }
        }

        if (!stopTheWorld) moveCarFromBeginingToEnd()
        canvas?.drawBitmap(currentCar, null, rectCar, null)
    }


    private val face by lazy {
        BitmapFactory.decodeResource(resources, R.drawable.face)
    }

    var faceXCenter = 0f
    var faceYCenter = 0f
    var faceSize = 150f.dpToPx()
    var faceEyeRadius = 20f.dpToPx()
    private fun drawFace(canvas: Canvas?) {

        val rectFace = RectF(
            canvasHalfWidth - faceSize / 2,
            canvasHeight - faceSize,
            canvasHalfWidth + faceSize / 2,
            canvasHeight
        )

        faceXCenter = rectFace.centerX()
        faceYCenter = rectFace.centerY()
        canvas?.drawBitmap(
            face,
            null,
            rectFace,
            null
        )
        when (degree) {
            180f -> canvas?.withScale(-1f, 1f, canvasHalfWidth, canvasHalfHeight) {
                drawEyes(canvas)
            }
            else -> drawEyes(canvas)
        }
        if (stopTheWorld) {
            drawPlease(canvas!!)
            return
        }
    }

    private fun drawEyes(canvas: Canvas?) {
        if (stopTheWorld) {
            return
        }
        //draw left eye
        val eye1X = faceXCenter - faceSize / 5 + 5f.dpToPx()
        val eye1Y = faceYCenter + 50f.dpToPx()
        canvas?.drawCircle(eye1X, eye1Y, faceEyeRadius, paintWhite)
        //draw left pupil
        var angle = getAngle(carXCenter, carYCenter, eye1X, eye1Y)
        var pinX = eye1X + faceEyeRadius / 2 * Math.sin(Math.toRadians(angle)).toFloat()
        var pinY = eye1Y + faceEyeRadius / 2 * Math.cos(Math.toRadians(angle)).toFloat()
        canvas?.drawCircle(pinX, pinY, faceEyeRadius / 2, paintBlack)
        //draw right eye
        val eye2X = faceXCenter + faceSize / 5 - 5f.dpToPx()
        val eye2Y = faceYCenter + 50f.dpToPx()
        canvas?.drawCircle(eye2X, eye2Y, faceEyeRadius, paintWhite)
        //draw right pupil
        angle = getAngle(carXCenter, carYCenter, eye2X, eye2Y)
        pinX = eye2X + faceEyeRadius / 2 * Math.sin(Math.toRadians(angle)).toFloat()
        pinY = eye2Y + faceEyeRadius / 2 * Math.cos(Math.toRadians(angle)).toFloat()
        canvas?.drawCircle(pinX, pinY, faceEyeRadius / 2, paintBlack)
    }

    private fun getAngle(point1X: Float, point1Y: Float, point2X: Float, point2Y: Float): Double {
        val deltaY = (point1Y - point2Y)
        val deltaX = (point2X - point1X)
        val result = Math.toDegrees(Math.atan2(deltaY.toDouble(), deltaX.toDouble()))
        return result - 90
    }

    init {
        initialAll()
    }
}