package com.example.customview

import android.content.Context
import android.content.res.Resources
import android.content.res.TypedArray
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.os.Build
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import androidx.core.content.ContextCompat
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import kotlin.coroutines.CoroutineContext
import kotlin.math.max

fun Float.dp2px(): Float {
    val r = Resources.getSystem()
    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, this, r.displayMetrics)
}

fun Float.dp2pxInt(): Int {
    return this.dp2px().toInt()
}

fun optPixelSize(typedArray: TypedArray?, index: Int, def: Float): Float {
    if (typedArray == null) return def
    return typedArray.getDimension(index, def)
}

fun optPixelSize(typedArray: TypedArray?, index: Int, def: Int): Int {
    if (typedArray == null) return def
    return typedArray.getDimensionPixelOffset(index, def)
}

fun optColor(typedArray: TypedArray?, index: Int, def: Int): Int {
    if (typedArray == null) return def
    return typedArray.getColor(index, def)
}

fun optBoolean(typedArray: TypedArray?, index: Int, def: Boolean): Boolean {
    if (typedArray == null) return def
    return typedArray.getBoolean(index, def)
}

class SwitchButton @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr),
    CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main
    val rootCoroutine = Job()
    val DEFAULT_WIDTH = 58f.dp2pxInt()
    val DEFAULT_HEIGHT = 36f.dp2pxInt()
    var borderWidth: Float
    var background: Int
    val rect = RectF()
    var shadowRadius: Int
    var shadowOffset: Int
    var shadowEffect: Boolean
    var shadowColor: Int
    var buttonRadius: Float = 0f
    var buttonMinX: Float = 0f
    var buttonMaxX: Float = 0f
    var buttonColor: Int = Color.WHITE
    var left: Float = 0f
    var right: Float = 0f
    var top: Float = 0f
    var bottom: Float = 0f
    var centerX: Float = 0f
    var centerY: Float = 0f
    var viewRadius: Float = 0f
    var myHeight: Float = 0f
    var myWidth: Float = 0f
    var checkLineWidth: Float = 0f
    var checkedLineOffsetX: Float = 0f
    var checkedLineOffsetY: Float = 0f
    var checkLineLenght: Float = 0f
    var uncheckColor: Int
    var checkedColor: Int
    var checkLineColor: Int
    var isUiInited = false
    var isChecked: Boolean = false
    var showIndicator: Boolean
    var viewState: ViewState = ViewState()
    var beforeState: ViewState = ViewState()
    var afterState: ViewState = ViewState()
    private val ANIMATE_STATE_NONE = 0
    private val ANIMATE_STATE_PENDING_DRAG = 1
    private val ANIMATE_STATE_DRAGING = 2
    private val ANIMATE_STATE_PENDING_RESET = 3
    private val ANIMATE_STATE_PENDING_SETTLE = 4
    private val ANIMATE_STATE_SWITCH = 5

    val paint by lazy {
        Paint(Paint.ANTI_ALIAS_FLAG).apply {
            isAntiAlias = true
        }
    }
    val buttonPaint by lazy {
        Paint(Paint.ANTI_ALIAS_FLAG).apply {
            isAntiAlias = true
            color = buttonColor
        }
    }

    init {
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.SwitchButton)
        this.setPadding(0, 0, 0, 0)

        shadowEffect = optBoolean(
            typedArray,
            R.styleable.SwitchButton_sb_shadow_effect,
            true
        )
        borderWidth = optPixelSize(
            typedArray,
            R.styleable.SwitchButton_sb_border_width,
            1f.dp2px()
        )
        background = optColor(
            typedArray,
            R.styleable.SwitchButton_sb_background,
            ContextCompat.getColor(context, R.color.n7red)
        )
        shadowRadius = optPixelSize(
            typedArray,
            R.styleable.SwitchButton_sb_shadow_radius,
            2.5f.dp2pxInt()
        )
        shadowOffset = optPixelSize(
            typedArray,
            R.styleable.SwitchButton_sb_shadow_offset,
            1.5f.dp2pxInt()
        )
        showIndicator = optBoolean(
            typedArray,
            R.styleable.SwitchButton_sb_show_indicator,
            true
        )
        uncheckColor = optColor(
            typedArray,
            R.styleable.SwitchButton_sb_border_width,
            0XffDDDDDD.toInt()
        )
        checkedColor = optColor(
            typedArray,
            R.styleable.SwitchButton_sb_checked_color,
            0Xff51d367.toInt()
        )
        checkLineColor = optColor(
            typedArray,
            R.styleable.SwitchButton_sb_checkline_color,
            Color.WHITE
        )
        isChecked = optBoolean(
            typedArray,
            R.styleable.SwitchButton_sb_checked,
            false
        )
        buttonColor = optColor(
            typedArray,
            R.styleable.SwitchButton_sb_button_color,
            Color.WHITE
        )
        shadowColor = optColor(
            typedArray,
            R.styleable.SwitchButton_sb_shadow_color,
            0X33000000
        )
        shadowRadius = optPixelSize(
            typedArray,
            R.styleable.SwitchButton_sb_shadow_radius,
            2.5f.dp2pxInt()
        )
        shadowOffset = optPixelSize(
            typedArray,
            R.styleable.SwitchButton_sb_shadow_offset,
            1.5f.dp2pxInt()
        )

        checkedLineOffsetX = 4f.dp2px()
        checkedLineOffsetY = 4f.dp2px()

        typedArray.recycle()

        if (shadowEffect) {
            buttonPaint.setShadowLayer(
                shadowRadius.toFloat(),
                0f, shadowOffset.toFloat(),
                shadowColor
            )
        }

        setLayerType(LAYER_TYPE_SOFTWARE, null)

    }

    override fun setPadding(left: Int, top: Int, right: Int, bottom: Int) {
        super.setPadding(0, 0, 0, 0)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        val heightMode = MeasureSpec.getMode(heightMeasureSpec)
        var newWidthMeasureSpec = widthMeasureSpec
        var newHeightMeasureSpec = heightMeasureSpec

        if (widthMode == MeasureSpec.UNSPECIFIED ||
            widthMode == MeasureSpec.AT_MOST
        ) {
            newWidthMeasureSpec = MeasureSpec.makeMeasureSpec(DEFAULT_WIDTH, MeasureSpec.EXACTLY)
        }

        if (heightMode == MeasureSpec.UNSPECIFIED ||
            heightMode == MeasureSpec.AT_MOST
        ) {
            newHeightMeasureSpec = MeasureSpec.makeMeasureSpec(DEFAULT_HEIGHT, MeasureSpec.EXACTLY)
        }


        super.onMeasure(newWidthMeasureSpec, newHeightMeasureSpec)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)

        val viewPadding = max(shadowRadius + shadowOffset.toFloat(), borderWidth)

        myHeight = h - viewPadding - viewPadding
        myWidth = w - viewPadding - viewPadding

        viewRadius = myHeight * .5f
        buttonRadius = viewRadius - borderWidth*3

        left = viewPadding
        top = viewPadding
        right = w - viewPadding
        bottom = h - viewPadding

        centerX = (left + right) * .5f
        centerY = (top + bottom) * .5f

        buttonMinX = left + viewRadius
        buttonMaxX = right - viewPadding

        if (isChecked) {
            setCheckedViewState(viewState)
        } else {
            setUncheckViewState(viewState)
        }

        isUiInited = true

        postInvalidate()
    }

    private fun setUncheckViewState(viewState: ViewState) {
        viewState.radius = 0f
        viewState.checkStateColor = uncheckColor
        viewState.checkedLineColor = Color.TRANSPARENT
        viewState.buttonX = buttonMinX
    }

    private fun setCheckedViewState(viewState: ViewState) {
        viewState.radius = viewRadius
        viewState.checkStateColor = checkedColor
        viewState.checkedLineColor = checkLineColor
        viewState.buttonX = buttonMaxX
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        // draw button background
        paint.strokeWidth = borderWidth
        paint.style = Paint.Style.FILL
        paint.color = background
        drawRoundRect(canvas, left, top, right, bottom, viewRadius, paint)

        // draw button shadow
        paint.style = Paint.Style.STROKE
        paint.color = uncheckColor
        drawRoundRect(canvas, left, top, right, bottom, viewRadius, paint)

        if (showIndicator) {
            drawCheckedIndicator(canvas)
        }

        val des = viewState.radius * .5f
        paint.style = Paint.Style.STROKE
        paint.color = viewState.checkStateColor
        paint.strokeWidth = borderWidth + des * 2f
        drawRoundRect(canvas, left + des, top + des, right - des, bottom - des, viewRadius, paint)

//        paint.style = Paint.Style.FILL
//        paint.strokeWidth = 1f
//        drawArc(canvas, left, top, left + 2 * viewRadius, top + 2 * viewRadius, 90f, 180f, paint)
//        canvas.drawRect(left + viewRadius, top, viewState.buttonX, top + 2 * viewRadius, paint)

        if (showIndicator) {
            drawCheckedIndicator(canvas)
        }

        drawButton(canvas, viewState.buttonX, centerY)
    }

    private fun drawButton(canvas: Canvas, x: Float, y: Float) {
        canvas.drawCircle(x, y, buttonRadius, buttonPaint)

//         draw shadow button
        paint.style = Paint.Style.STROKE
        paint.strokeWidth = 1f
        paint.color = 0XffDDDDDD.toInt()
        canvas.drawCircle(x, y, buttonRadius, paint)
    }

    private fun drawArc(canvas: Canvas, left: Float, top: Float, right: Float, bottom: Float, startAngle: Float, sweepAngle: Float, paint: Paint) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            canvas.drawArc(left, top, right, bottom, startAngle, sweepAngle, true, paint)
        } else {
            rect.set(left, top, right, bottom)
            canvas.drawArc(rect, startAngle, sweepAngle, true, paint)
        }
    }

    private fun drawRoundRect(
        canvas: Canvas,
        left: Float, top: Float,
        right: Float, bottom: Float,
        backgroundRadius: Float,
        paint: Paint
    ) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            canvas.drawRoundRect(
                left,
                top,
                right,
                bottom,
                backgroundRadius,
                backgroundRadius,
                paint
            )
        } else {
            rect.set(left, top, right, bottom)
            canvas.drawRoundRect(rect, backgroundRadius, backgroundRadius, paint)
        }
    }

    private fun drawCheckedIndicator(canvas: Canvas) {
        drawCheckedIndicator(
            canvas,
            viewState.checkedLineColor,
            checkLineWidth,
            left + viewRadius - checkedLineOffsetX, centerX - checkLineLenght,
            left + viewRadius - checkedLineOffsetY, centerY + checkLineLenght,
            paint
        )
    }

    private fun drawCheckedIndicator(
        canvas: Canvas,
        color: Int,
        lineWidth: Float,
        sx: Float, sy: Float, ex: Float, ey: Float,
        paint: Paint
    ) {
        paint.style = Paint.Style.STROKE
        paint.color = color
        paint.strokeWidth = lineWidth
        canvas.drawLine(sx, sy, ex, ey, paint)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        coroutineContext.cancel()
    }

    class ViewState {
        var buttonX: Float = 0f
        var checkStateColor: Int = 0
        var checkedLineColor: Int = 0
        var radius: Float = 0f

        private fun copy(source: ViewState) {
            this.buttonX = source.buttonX
            this.checkStateColor = source.checkStateColor
            this.checkedLineColor = source.checkedLineColor
            this.radius = source.radius
        }
    }

}