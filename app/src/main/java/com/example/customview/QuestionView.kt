package com.example.customview

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.CheckBox
import android.widget.LinearLayout
import android.widget.TextView

class QuestionView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    lateinit var tvQuestion: TextView
    lateinit var cbQuestion: CheckBox
    lateinit var cb2Question: CheckBox
    private var questionText = ""

    init {
        LayoutInflater.from(context).inflate(R.layout.compound_view, this, true)

        val styleAttributeSet = context.obtainStyledAttributes(attrs, R.styleable.QuestionView)
        questionText = styleAttributeSet.getString(R.styleable.QuestionView_questionText) ?: ""
        styleAttributeSet.recycle()
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        tvQuestion = findViewById(R.id.tv_compound_view)
        cbQuestion = findViewById(R.id.cb_compound_view)
        cbQuestion.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) cb2Question.isChecked = false
        }
        cb2Question = findViewById(R.id.cb2_compound_view)
        cb2Question.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) cbQuestion.isChecked = false
        }
        tvQuestion.text = questionText
    }

    fun setQuestionText(text: String) {
        questionText = text
        tvQuestion.text = questionText
    }


}