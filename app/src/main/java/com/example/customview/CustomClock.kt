package com.example.customview

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.graphics.withTranslation
import java.text.SimpleDateFormat
import java.util.*
import android.graphics.Typeface




class CustomClock @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    fun Float.dpToPx(): Float {
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            this,
            resources.displayMetrics
        )
    }

    fun Float.spToPx(): Float {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, this, resources.displayMetrics)
    }

    fun getApproxXToCenterText(text: String, typeface: Typeface, fontSize: Float, widthToFitStringInto: Float
    ): Int {
        val p = Paint()
        p.typeface = typeface
        p.textSize = fontSize
        val textWidth = p.measureText(text)
        return ((widthToFitStringInto - textWidth) / 2f).toInt() - (fontSize / 2f).toInt()
    }


    var weigthBar = 100f
    val innerPadding = 20f.dpToPx()
    val strokeGrid = 2f.dpToPx()

    var gridLeft = 0f
    var gridBottom = 0f
    var gridTop = 0f
    var gridRight = 0f
    var portion = 0f

    var milis = 0
    var seconds = 0
    var minutes = 0
    var hours = 0

    var stringDate = ""

    init {
        val styleAttributeSet = context.obtainStyledAttributes(attrs,R.styleable.CustomClock)
        weigthBar = styleAttributeSet.getDimension(R.styleable.CustomClock_width_column, 200f)

        styleAttributeSet.recycle()
    }

    val paintHours = Paint().apply {
        style = Paint.Style.FILL_AND_STROKE
        isAntiAlias = true
        color = (Color.RED)
        strokeWidth = 2f.dpToPx()

    }
    val gridPaint = Paint().apply {
        color = (Color.BLACK)
        strokeWidth = strokeGrid
        style = Paint.Style.STROKE
    }
    val paintGuideLine = Paint().apply {
        color = (Color.GRAY)
        strokeWidth = 0.5f.dpToPx()
        style = Paint.Style.STROKE
        pathEffect = DashPathEffect(floatArrayOf(30f, 10f), 10f)
    }
    val paintDate = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = (Color.BLACK)
        textSize = 20f.spToPx()
        typeface = Typeface.DEFAULT
        style = Paint.Style.STROKE
    }
    val paintTextColumn = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = (Color.BLACK)
        textSize = 13f.spToPx()
        typeface = Typeface.DEFAULT
        style = Paint.Style.STROKE
        textAlign = Paint.Align.CENTER
    }
    val paintTextOnLine = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        textSize = 11f.spToPx()
        color = Color.GRAY
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        gridLeft = innerPadding + leftPaddingOffset
        gridBottom = height - innerPadding - bottomPaddingOffset
        gridTop = innerPadding + topPaddingOffset
        gridRight = width - innerPadding - rightPaddingOffset
    }



    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        initTime()

        // draw time
        canvas?.drawText(stringDate, getApproxXToCenterText(stringDate, Typeface.DEFAULT, 20f.spToPx(),width.toFloat()).toFloat(), paintDate.textSize, paintDate)

        drawGrid(canvas)

        canvas?.withTranslation(width / 10f, 0f) {
            canvas.withTranslation(0f, 0f) {
                drawColumn(canvas,"hours", hours * portion, R.color.purple_900)
            }
            canvas.withTranslation(width / 5f, 0f) {
                drawColumn(canvas, "min",minutes * portion, R.color.purple_700)
            }
            canvas.withTranslation(width / 5f * 2, 0f) {
                drawColumn(canvas, "sec",seconds * portion, R.color.purple_500)
            }
            canvas.withTranslation(width / 5f * 3, 0f) {
                drawColumn(canvas, "",milis * portion, R.color.purple_300)
            }
        }

        val delay = 200L
//        postInvalidateDelayed(delay)
        postInvalidateOnAnimation()
    }

    @SuppressLint("SimpleDateFormat")
    private fun initTime() {
        val calendar = Calendar.getInstance()

        stringDate = SimpleDateFormat("MMM dd yyyy HH:mm:ss").format(calendar.timeInMillis)

        milis = calendar.get(Calendar.MILLISECOND) / 10
        seconds = calendar.get(Calendar.SECOND)
        minutes = calendar.get(Calendar.MINUTE)
        hours = calendar.get(Calendar.HOUR_OF_DAY)
    }

    private fun drawGrid(canvas: Canvas?) {
        // draw grid lines
        canvas?.drawLine(gridLeft, gridBottom, gridLeft, gridTop, gridPaint)
        canvas?.drawLine(gridLeft, gridBottom, gridRight, gridBottom, gridPaint)
        // draw guide lines
        val percent100 = 100f
        portion = (gridBottom - gridTop) / percent100
        var y: Float
        (1..9).forEach {
            y = gridBottom - it * portion * 10
            val path = Path()
            path.moveTo(gridLeft, y)
            path.lineTo(gridRight, y)
            canvas?.drawPath(path, paintGuideLine)
            canvas?.drawTextOnPath(
                (it * 10).toString(),
                path,
                10f,
                -5f,
                paintTextOnLine
            )
        }
    }

    private fun drawColumn(canvas: Canvas?,columnName :String, desiredHeight: Float, color: Int) {
        val heightBarHours = gridBottom - desiredHeight
        val paint = paintHours
        val gap = 5f.dpToPx()
        paint.color = ContextCompat.getColor(context, color)
        canvas?.drawRoundRect(
            gridLeft,
            heightBarHours,
            weigthBar,
            gridBottom - strokeGrid,
            0f,
            0f,
            paint
        )

        canvas?.drawText(columnName,(gridLeft + weigthBar )/2, heightBarHours - gap , paintTextColumn)

    }

}